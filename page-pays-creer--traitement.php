<?php
  define("PAGE_TITLE", "Traitement");
  require("inc/inc.kickstart.php");
?>
<main class="pays-creer">
<?php
  if(isset($_POST["country_name"]) && isset($_POST["country_flag"]) && isset($_POST["country_capital"]) && isset($_POST["country_area"])){
    $country_name = htmlspecialchars(strip_tags($_POST["country_name"]));
    $country_flag = htmlspecialchars(strip_tags($_POST["country_flag"]));
    $country_capital = htmlspecialchars(strip_tags($_POST["country_capital"]));
    $country_area = htmlspecialchars(strip_tags(intval($_POST["country_area"])));
  }
  try{
    $requete = "INSERT INTO `country` (`country_name`, `country_flag`, `country_capital`, `country_area`) 
                VALUES (:country_name, :country_flag, :country_capital, :country_area)";
    $etape = $pdo->prepare($requete);
    $etape->bindParam(':country_name', $country_name);
    $etape->bindParam(':country_flag', $country_flag);
    $etape->bindParam(':country_capital', $country_capital);
    $etape->bindParam(':country_area', $country_area);
    $etape->execute();
  }
  catch(PDOException $e){
    echo $e->getMessage();
  }
  
  echo "<h3>Merci !</h3>";
  echo "<p>Voici un récapitulatif de votre contribution :</p>";
  echo "<ul>"
      ."<li>Nom du pays : " . $country_name . "</li>"
      ."<li>Capitale du pays : " . $country_capital . "</li>"
      ."<li>Drapeau du pays : " . $country_flag . "</li>"
      ."<li>Superficie du pays (en km²) : " . $country_area . "</li>"
      ."<ul>";
  echo "<a href='page-pays-liste-alpha.php'><button>Consulter la liste des pays</button></a>";
?>
</main>
<?php require("inc/inc.footer.php"); ?>