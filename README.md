BRIEF 3 • Afrique
=================
Bienvenue en Afrique, un brief orienté sécurité (XSS, Injection SQL) dans lequel l'utilisateur peut lister tous les pays d'Afrique et en ajouter un nouveau.

Contexte
--------
L'agence qui a développé ce petit site pour une école n'avait pas pensé à le sécuriser contre les attaques de type "Injection SQL" et "XSS". Du coup, l'école vous demande de développer les parades adéquates.

Matériel fourni
---------------
- Une table MySQL contenant 54 entrées : `brief3-bdd.sql`
- Une archive des fichiers & dossiers du site, tel qu'il a été livré par l'agence : `brief3-afrique-v1.7z`

Contraintes
-----------
Dans ce brief, l'apprenant ne touche qu'aux fichiers PHP.

Objectifs
---------
1. Dans le répertoire parent du site, créer un fichier `inc.bdd.php` contenant les constantes `DB_SERVER`, `DB_DATABASE`, `DB_USERNAME` et `DB_PASSWORD`. Y renseigner les valeurs adéquates.
2. Développer le code nécessaire à la gestion des erreurs de connexion à la base de données.
3. Développer le code nécessaire à la parade des failles de types "Cross Site Scripting" (XSS) présentes sur la page `page-pays-liste-alpha.php`.
4. Développer le code nécessaire au tri alphabétique des noms de pays, sur la page `page-pays-liste-alpha.php`.
5. Développer le code nécessaire à la parade des failles de types "Cross Site Scripting" (XSS) présentes sur la page `page-pays-creer--traitement.php`.
6. Développer le code nécessaire à la parade de la faille de type "Injection SQL" présentes sur la page `page-pays-creer--traitement.php`.

Intension
---------
De plus en plus amené à contribuer aux sites Internet qui permettent de gérer des données, il est nécessaire pour un développeur d’être capable d'en sécuriser l'accès, la gestion et l'affichage.

Compétences concernées
---------
- C2: Créer une interface web statique et adaptable
- C3: Développer une interface web dynamique
- C6: Développer les composants d’accès aux données
- C7: Développer la partie back-end d’une application web ou web mobile

Livrables
---------
Le lien vers le GitLab privé de l'apprenant est à envoyer par mail aux deux formateurs :
- Jérémie : jcanavesio@simplon.co
- Géna : gpailha@simplon.co

Critères de réussite
--------------------
- Les objectifs listés dans ce brief sont tous remplis
- Les fichiers sont déposés dans un repo GitLab
- Les formateurs ont un accès développeur au repo Gitlab, sans limite de temps
- Le lien vers le Gitlab est envoyé aux deux formateurs par mail

Deadline
--------
Travail individuel à livrer avant le 07/08/2020 à 11h59.